package it.tdgroup.learning.userchallenge.utente.api;

import it.tdgroup.learning.userchallenge.utente.dto.UtenteDTO;
import it.tdgroup.learning.userchallenge.utente.response.InsertUtenteResponse;
import it.tdgroup.learning.userchallenge.utente.service.UtenteService;
import it.tdgroup.learning.userchallenge.util.exception.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("utenti")
public class UtenteAPI {

    @Autowired
    UtenteService utenteService;

    @RequestMapping(value = "/insertUtente", method = RequestMethod.POST)
    public ResponseEntity<InsertUtenteResponse> insertUtente(
            @RequestBody UtenteDTO utenteDTO) throws ApplicationException {
        try {
            InsertUtenteResponse response = utenteService.insertUser(utenteDTO);
            return new ResponseEntity<>(response, HttpStatus.CREATED);

        } catch (Exception ex) {
            throw new ApplicationException(
                    "[UtenteAPI].insertUtente: Application Exception occurred: " + ex.getMessage());
        }
    }
}
