package it.tdgroup.learning.userchallenge.utente.repository;

import it.tdgroup.learning.userchallenge.utente.entity.Utente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtenteRepository extends JpaRepository<Utente, Integer> {

}
