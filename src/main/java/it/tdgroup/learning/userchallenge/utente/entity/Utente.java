package it.tdgroup.learning.userchallenge.utente.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Table(name = "UTENTE")
@Entity
@Data
public class Utente {
    @Id
    @SequenceGenerator(name = "UTENTE_SEQ_GENERATOR", sequenceName = "SEQ_UTENTE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UTENTE_SEQ_GENERATOR")
    @Column(name = "ID_UTENTE", nullable = false)
    private Integer idUtente;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "COGNOME")
    private String cognome;

    @Column(name = "DATA_NASCITA")
    private Date dataNascita;

    @Column(name = "CODICE_FISCALE")
    private String codiceFiscale;
}
