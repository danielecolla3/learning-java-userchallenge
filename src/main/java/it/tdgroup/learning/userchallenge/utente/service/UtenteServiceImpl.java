package it.tdgroup.learning.userchallenge.utente.service;

import it.tdgroup.learning.userchallenge.utente.dto.UtenteDTO;
import it.tdgroup.learning.userchallenge.utente.entity.Utente;
import it.tdgroup.learning.userchallenge.utente.mapper.UtenteMapper;
import it.tdgroup.learning.userchallenge.utente.repository.UtenteRepository;
import it.tdgroup.learning.userchallenge.utente.response.InsertUtenteResponse;
import it.tdgroup.learning.userchallenge.util.Constants;
import it.tdgroup.learning.userchallenge.util.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UtenteServiceImpl implements UtenteService {

    @Autowired
    UtenteMapper utenteMapper;

    @Autowired
    UtenteRepository utenteRepository;

    @Override
    public InsertUtenteResponse insertUser(UtenteDTO utenteDTO) throws ServiceException {
        try {
            Utente utente = utenteMapper.convertDtoToEntity(utenteDTO);
            utenteRepository.save(utente);

            InsertUtenteResponse response = new InsertUtenteResponse();
            response.setIdUtente(utente.getIdUtente());
            response.setMessaggio(Constants.UTENTE_INSERITO);
            return response;
        } catch (Exception ex) {
            throw new ServiceException("[UtenteServiceImpl].insertUser: " + ex.getMessage());
        }
    }
}
