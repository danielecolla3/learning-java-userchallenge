package it.tdgroup.learning.userchallenge.utente.service;

import it.tdgroup.learning.userchallenge.utente.dto.UtenteDTO;
import it.tdgroup.learning.userchallenge.utente.response.InsertUtenteResponse;
import it.tdgroup.learning.userchallenge.util.exception.ServiceException;

public interface UtenteService {
    InsertUtenteResponse insertUser(UtenteDTO utenteDTO) throws ServiceException;
}
