package it.tdgroup.learning.userchallenge.utente.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class UtenteDTO {
    private Integer idUtente;
    private String nome;
    private String cognome;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "CET")
    private Date dataNascita;
    private String codiceFiscale;
}
