package it.tdgroup.learning.userchallenge.utente.response;

import lombok.Data;

@Data
public class InsertUtenteResponse {
    private String messaggio;
    private Integer idUtente;
}
