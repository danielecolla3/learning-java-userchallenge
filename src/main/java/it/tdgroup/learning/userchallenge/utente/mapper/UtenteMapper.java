package it.tdgroup.learning.userchallenge.utente.mapper;

import it.tdgroup.learning.userchallenge.utente.dto.UtenteDTO;
import it.tdgroup.learning.userchallenge.utente.entity.Utente;
import it.tdgroup.learning.userchallenge.util.AbstractMapperComponent;
import it.tdgroup.learning.userchallenge.util.exception.MapperException;
import org.springframework.stereotype.Component;

@Component
public class UtenteMapper extends AbstractMapperComponent<UtenteDTO, Utente> {
    @Override
    public UtenteDTO convertEntityToDto(Utente entity) throws MapperException {
        try {
            if (entity != null) {
                UtenteDTO utenteDTO = new UtenteDTO();
                utenteDTO.setIdUtente(entity.getIdUtente());
                utenteDTO.setNome(entity.getNome());
                utenteDTO.setCognome(entity.getCognome());
                utenteDTO.setCodiceFiscale(entity.getCodiceFiscale());
                utenteDTO.setDataNascita(entity.getDataNascita());

                return utenteDTO;
            }
            return null;

        } catch (Exception ex) {
            throw new MapperException("[UtenteMapper].convertEntityToDto: " + ex.getMessage());
        }
    }

    @Override
    public Utente convertDtoToEntity(UtenteDTO dto) throws MapperException {
        try {
            if (dto != null) {
                Utente entity = new Utente();
                entity.setNome(dto.getNome());
                entity.setCognome(dto.getCognome());
                entity.setDataNascita(dto.getDataNascita());
                entity.setCodiceFiscale(dto.getCodiceFiscale());

                return entity;
            }
            return null;

        } catch (Exception ex) {
            throw new MapperException("[UtenteMapper].convertDtoToEntity: " + ex.getMessage());
        }
    }
}
